

    template <class T> void List<T>::AddItem(T item)
    {
        Link<T> *link= new Link<T>;
        link->_item=item;
        if (!_head)
        {
            link->previous=NULL;
            link->next=NULL;
            _head=link;
            _tail=link;
            _current=link;
        }
        else
        {
            link->previous=_tail;
            link->next=NULL;
            _tail->next=link;
            _tail=link;
        }
        _size++;
    }


    template <class T> void List<T>::RemoveItem(T item)
    {
        Link<T> *link=_head;
        while (link)
        {
            if (link->_item==item)
            {
                if (link->next)
                    link->next->previous=link->previous;
                else
                {
                    _tail=link->previous;
                    _tail->next=NULL;
                }
                if (link->previous)
                    link->previous->next=link->next;
                else
                {
                    _head=link->next;
                    _head->previous=NULL;
                }
                delete link;
                break;
            }
            link=link->next;
        }
        _size--;
    }
    template <class T> int List<T>::size(){return _size;}
    template <class T> T List<T>::Head(){ _current=_head; return _current->_item;}
    template <class T> T List<T>::Tail(){ _current=_tail; return _current->_item;}
    template <class T> void List<T>::Prev(){_current=_current->previous;}
    template <class T> void List<T>::Next(){_current=_current->next;}
    template <class T> T List<T>::Current(){  return _current->_item;}



