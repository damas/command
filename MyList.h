template <class T> class Link{
public:
    Link* previous;
    T _item;
    Link* next;
};



template <class T> class List{
    public:
        List::List(){_head = NULL; _tail=NULL; _current=NULL; _size=0;};
        void AddItem(T);
        void RemoveItem(T);
        int size() ;
        T Head();
        T Tail();
        void Next();
        void Prev();
        T Current();
    private:
        Link<T> *_head;
        Link<T> *_tail;
        Link<T> *_current;
        int _size;
    };
#include "MyList.cpp"