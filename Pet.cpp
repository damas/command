
Pet::Pet(string name){
    _name = name;
    _satiety=80;
    _health=80;
    _mood=80;
    _isDied=false;
};
void Pet::Restart(){
    _satiety=80;
    _health=80;
    _mood=80;
    _isDied=false;
}
void Pet::Feed(){
    if (_isDied) return;
    if (_satiety>90) cout<<"I'm not hungry\n" ;
    else _satiety+=20;
    if (_satiety>100) _satiety=100;
}
void Pet::Cure(){
    if (_isDied) return;
    if (_health>90) cout<<"I'm feeling good\n" ;
    else _health+=20;
    if (_health>100) _health=100;
}
void Pet::Play(){
    if (_isDied) return;
    _mood+=20;
    if(_mood>100) _mood=100;
}
string Pet::Statement(){
    if (_isDied)
        return "died\n";
    else return "breathing\n";
}
int Pet::AppStatement(int time){
    if (_isDied) return 1;
    _health-=time;
    _satiety-=time;
    _mood-=time;
    if (_health<0) _health=0;
    if (_satiety<0) _satiety=0;
    if (_mood<0) _mood=0;
    if (_health&&_satiety&&_mood)
        return 0;
    else _isDied=true;
    return 1;
}

