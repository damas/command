// Tammagochi.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include "MyList.h"
#include "Pet.h"

//������� �����
class Command{
public:
    ~Command(){};
    virtual void execute()=0;
protected:
    Command(){};
};
//���������� ������
class FeedCommand : public Command{
public:
    FeedCommand(Pet *pgame){_pgame=pgame;};
    virtual void execute();
private:
    Pet *_pgame;
};

void FeedCommand::execute() {
    _pgame->Feed();
};
class CureCommand: public Command{
public:
    CureCommand(Pet* );
    virtual void execute();
private:
    Pet *_pgame;
};

CureCommand::CureCommand(Pet *pgame){_pgame = pgame;}

void CureCommand::execute() {
    _pgame->Cure();
};

class PlayCommand: public Command{
public:
    PlayCommand(Pet*);
    virtual void execute();
private:
    Pet *_pgame;
};

PlayCommand::PlayCommand(Pet *pgame){_pgame = pgame;}

void PlayCommand::execute() {
    _pgame->Play();
};

class InfoCommand: public Command{
public:
    InfoCommand(Pet*);
    virtual void execute();
private:
    Pet *_pgame;
};

InfoCommand::InfoCommand(Pet *pgame){_pgame = pgame;}

void InfoCommand::execute() {
    cout<<_pgame->GetName()<<" is "<<_pgame->Statement();
};

class UpdateCommand: public Command{
public:
    UpdateCommand(Pet*, int);
    virtual void execute();
private:
    Pet *_pgame;
    int _time;
};

UpdateCommand::UpdateCommand(Pet *pgame, int time){_pgame = pgame; _time=time;}

void UpdateCommand::execute() {
    _pgame->AppStatement(_time);
};

class RestartCommand: public Command{
    public:
    RestartCommand(Pet*);
    virtual void execute();
private:
    Pet *_pgame;
};

RestartCommand::RestartCommand(Pet *pgame){_pgame = pgame;}

void RestartCommand::execute() {
    _pgame->Restart();
}

//���������� ������ ������
class MacroCommand: public Command{
public:
    MacroCommand(){_cmds=new List<Command*>();};
    virtual ~MacroCommand(){};
    void add(Command*);
    void remove(Command*);
    virtual void execute();
private:
    List<Command*>* _cmds;
};
    
void MacroCommand::add(Command *c)
{
    _cmds->AddItem(c);
}

void MacroCommand::remove(Command *c)
{
    _cmds->RemoveItem(c);
}
void MacroCommand::execute(){
    if(_cmds->size()){
        Command *c=_cmds->Head();
        for (int i=1; i<=_cmds->size(); i++){
            c=_cmds->Current();
            c->execute();
            _cmds->Next();
        }
    }
}




int _tmain(int argc, _TCHAR* argv[])
{
    Pet cat("Murzic");
    MacroCommand macro;
    //��� ���?
    macro.add(new InfoCommand(&cat));
    //�������� ����
    macro.add(new FeedCommand(&cat));
    //������� ���������
    macro.add(new CureCommand(&cat));
    //�������� � �����
    macro.add(new PlayCommand(&cat));
    //��� ��� ��������
    macro.add(new FeedCommand(&cat));
    //���� �� 3 ���
    Command *update=new UpdateCommand(&cat, 72);
    macro.add(update);
    for (int i=0; i<3; i++){
        cout<<3*i+1<<"day: ";
        macro.execute();
    }
    InfoCommand info(&cat);
    //������� ���������
    cout<<"10day: ";
    info.execute();

    //��� ����. ��������� ��� ���
    cout<<endl<<"Try again!"<<endl;
    RestartCommand res(&cat);
    res.execute();

    //����� ������ ��������� ����?
    macro.remove(update);
    delete update;
    update=new UpdateCommand(&cat, 24) ;
    macro.add(update);
    for (int i=0; i<5; i++){
        cout<<2*i+1<<"day: ";
        macro.execute();
    }
    update->execute();
    cout<<"10day: ";
    info.execute();
   

    return 0;
}
