#include <iostream>
#include <string>
using namespace std;
class Pet{
public:
    Pet(string name);
    void Restart();
    void Feed();
    void Cure();
    void Play();
    string Statement();
    int AppStatement(int time);
    string GetName(){return _name;};
private:
    string _name;
    int _satiety;
    int _health;
    int _mood;
    bool _isDied;//����?
};
#include "Pet.cpp"